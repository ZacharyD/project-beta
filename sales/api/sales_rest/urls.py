from django.urls import path
from .views import (
    api_sales_person_list,
    api_sales_person_details,
    api_customers,
    api_customer_details,
    api_sales_records,
    api_sales_record_details,
    api_sales_person_records,
    api_list_automobiles,
)
 

urlpatterns = [
    path("employees/", api_sales_person_list, name="api_sales_person"),
    path("employees/<int:id>/", api_sales_person_details, name="api_sales_person_details"),
    path("customers/", api_customers, name="api_customers"),
    path("customers/<int:id>/", api_customer_details, name="api_customer_details"),
    path("sales/", api_sales_records, name="api_sales_records"),
    path("sales/<int:id>/", api_sales_record_details, name="api_sales_record_details"),
    path("employee/<int:id>/sales/", api_sales_person_records, name="api_sales_person_records"),
    path("autos/", api_list_automobiles, name="api_list_automobiles"),
]
