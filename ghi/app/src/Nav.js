import { NavLink } from 'react-router-dom';
import './index.css'

function Nav() {
  return (
    <nav className="row navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid col-md-8 offset-md-3">
        <NavLink className="navbar-brand" to="/">Automobile Service Industry</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className='nav-item'>
              <NavLink className='nav-link active' aria-current='page' to='/'>Home</NavLink>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Appointment</a>
              <ul class="dropdown-menu">
                <li><a class="dropdown-item" href='/appointmentform'>Add Appointment</a></li>
                <li><a class="dropdown-item" href='/appointmentlist'>View Appointments</a></li>
              </ul>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="true">Customer</a>
              <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="/customer/new">Add Customer</a></li>
                <li><a class="dropdown-item" href="/customers">View Customers</a></li>
              </ul>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Sales</a>
              <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="/sales/members/new">Add Sales Member</a></li>
                <li><a class="dropdown-item" href="/sales/members/list">View Sales Team</a></li>
                <li><a class="dropdown-item" href="/sales/members">Sales Team Accomplishments</a></li>
                <li><hr class="dropdown-divider" /></li>
                <li><a class="dropdown-item" href="/sales/records">Add Sales Record</a></li>
                <li><a class="dropdown-item" href="/sales">View Sales List</a></li>
              </ul>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="true">Service</a>
              <ul class="dropdown-menu">
                <li><a class="dropdown-item" href='/technicianform'>Technician Form</a></li>
                <li><a class="dropdown-item" href='/servicehistoryform'>Service History</a></li>
              </ul>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Manufacturer</a>
              <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="/manufacturers/new">Create Manufacturer</a></li>
                <li><a class="dropdown-item" href="/manufacturers">List Manufacturers</a></li>
              </ul>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Models</a>
              <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="/models/new">Create Model</a></li>
                <li><a class="dropdown-item" href="/models">List Models</a></li>
              </ul>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Vehicles</a>
              <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="/vehicles/new">Create Vehicle</a></li>
                <li><a class="dropdown-item" href="/vehicles">List Vehicles</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
};

export default Nav;

// import { NavLink } from 'react-router-dom';

// function Nav() {
//   return (
//   <nav className="navbar navbar-expand-lg navbar-dark bg-success">
//       <div className="container-fluid">
//           <NavLink className="navbar-brand" to="/">CarCar</NavLink>
//           <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
//               <span className="navbar-toggler-icon"></span>
//           </button>
//           <div className="collapse navbar-collapse" id="navbarSupportedContent">
//               <ul className="navbar-nav me-auto mb-2 mb-lg-0">
//                   <li className='nav-item'>
//                   <NavLink className='nav-link active' aria-current='page' to='/'>Home</NavLink>
//                   </li>
//                   <li className='nav-item'>
//                       <div className="nav-link active dropdown-toggle" data-toggle="dropdown">Service Management</div>
//                       <div className="dropdown-menu">
//                       <NavLink className='dropdown-item' to='/technicianform'>Technician Form</NavLink>
//                       <NavLink className='dropdown-item' to='/appointmentform'>Appointment Form</NavLink>
//                       <NavLink className='dropdown-item' to='/appointmentlist'>Appointment List</NavLink>
//                       <NavLink className='dropdown-item' to='/servicehistoryform'>Service History Form</NavLink>
//                       </div>
//                   </li>
//                   <li className='nav-item'>
//                       <div className="nav-link active dropdown-toggle" data-toggle="dropdown">Customer and Sales</div>
//                       <div className="dropdown-menu">
//                       <NavLink className="dropdown-item" to="/customer/new">Add Customer</NavLink>
//                       <NavLink className="dropdown-item" to="/customers">Customer List</NavLink>
//                       <NavLink className="dropdown-item" to="/sales/members/new">Add Sales Member</NavLink>
//                       <NavLink className="dropdown-item" to="/sales/members/list">View Sales Team</NavLink>
//                       <NavLink className="dropdown-item" to="/sales/members">Sales Team Accomplishments</NavLink>
//                       <NavLink className="dropdown-item" to="/sales/records">Add Sales Record</NavLink>
//                       <NavLink className="dropdown-item" to="/sales">Sales List</NavLink>
//                       </div>
//                   </li>
//                   <li className='nav-item'>
//                       <div className="nav-link active dropdown-toggle" data-toggle="dropdown">Vehicle Management</div>
//                       <div className="dropdown-menu">
//                       <NavLink className="dropdown-item" to="/manufacturers">Manufacturers</NavLink>
//                       <NavLink className='dropdown-item' to="manufacturers/new">Create Manufacturer</NavLink>
//                       <NavLink className='dropdown-item' to="/models">Models</NavLink>
//                       <NavLink className='dropdown-item' to="models/new">Create Model</NavLink>
//                       <NavLink className='dropdown-item' to="/vehicles">Vehicles</NavLink>
//                       <NavLink className='dropdown-item' to="vehicles/new">Create Vehicle</NavLink>
//                   </div>
//                   </li>
//               </ul>
//           </div>
//       </div>
//   </nav>
//   )
// }

// export default Nav;
