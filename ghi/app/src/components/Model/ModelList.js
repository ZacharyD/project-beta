import { useState, useEffect } from "react"
import { NavLink } from "react-router-dom"
import { useParams } from 'react-router-dom'

function ModelList() {
    const [models, setModels] = useState([])
    const { id } = useParams()

    const getData = async () => {
        const url = 'http://localhost:8100/api/models/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setModels(data.models)
        }
    }

    const handleDelete = async (id) => {
        const response = await fetch(`http://localhost:8100/api/models/${id}`, {
            method: "DELETE",
        })

        if (response.ok) {
            setModels(models.filter((model) => model.id !== id))
        }
    }

    useEffect(() => {
        getData()
    }, [])

    return (
        <div>
            <h1 className="text-center">Automobile Models</h1>
            <table className="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Models</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(model => {
                        return (
                            <tr key={model.id} className="card col p-3 shadow">
                                <td>{model.name}</td>
                                <td>
                                <button onClick={() => handleDelete(model.id)}>Delete</button>
                                </td>
                            </tr>
                        )
                        })}
                </tbody>
            </table>
                <div>
                <NavLink className="btn btn-primary" to="/models/new">Create</NavLink>
                </div>
        </div>
    )
}

export default ModelList;
