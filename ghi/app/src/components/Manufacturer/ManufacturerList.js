import { useState, useEffect } from "react"
import { NavLink } from "react-router-dom"

function ManufacturerList() {
    const [manufacturers, setManufacturers] = useState([])

    const getData = async () => {
        const url = 'http://localhost:8100/api/manufacturers'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setManufacturers(data.manufacturers)
        }
    }

    const handleDelete = async (id) => {
        const response = await fetch(`http://localhost:8100/api/manufacturers/${id}`, {
            method: "DELETE",
        })
        if (response.ok) {
            setManufacturers(manufacturers.filter((manufacturer) => manufacturer.id !== id))
        }
    }

    useEffect(() => {
        getData()
    }, [])

    return (
        <div>
            <h1 className="text-center">Manufacturer</h1>
            <table className="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map(manufacturer => {
                        return (
                            <tr key={manufacturer.id} className="card col p-3 shadow">
                                <td>{manufacturer.name}</td>
                                <td>
                                <button onClick={() => handleDelete(manufacturer.id)}>Delete</button>
                                </td>
                            </tr>
                        )
                        })}
                </tbody>
            </table>
                <div>
                    <NavLink className="btn btn-primary" to="/manufacturers/new">Create</NavLink>
                </div>
        </div>
    )
}

export default ManufacturerList;
