import { useState } from "react"

function ManufacturerForm() {
    const [formData, setFormData] = useState({})
    const handleFormData = e => {
        setFormData({
            ...formData,
            [e.target.name]:e.target.value
        })
    }

    const handleSubmit = async event => {
        event.preventDefault()
        const url = 'http://localhost:8100/api/manufacturers/'
        const fetchData = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {"Content-Type":"application/json"}
        }
        const response = await fetch(url, fetchData)
        if (response.ok) {
            const newResponse = await response.json()
            setHasSignedUp(true)
        } else {
            setHasFailed(true)
        }
    }

    const [hasSignedUp, setHasSignedUp] = useState(false)
    const formClasses = (!hasSignedUp) ? '' : 'd-none';
    const messageClasses = (!hasSignedUp) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';

    const [hasFailed, setHasFailed] = useState(false)
    const formClass = (!hasFailed) ? '' : 'd-none';
    const messageClass=(!hasFailed) ? 'alert alert-warning d-none mb-0' : 'alert alert-danger mb-0';

    return (
        <div className="container shadow">
            <div className="card shadow p-3">
                <form className={formClasses - formClass} onSubmit={handleSubmit}>
                    <h1 className="text-center">New Manufacturer</h1>
                    <div className="form-floating row">
                        <input onChange={handleFormData} className="form-control" required name="name"></input>
                        <label>Manufacturer</label>
                    </div>
                    <div className="p-2 row">
                        <button className="btn btn-primary">Save</button>
                    </div>
                </form>
                <div className={messageClasses}  id="success-message">
                    <p className="text-center">New manufacturer added!</p>
                </div>
                <div className={messageClass} id="warning-message">
                    <p className="text-center">Manufacturer Already Exists</p>
                </div>
            </div>
        </div>
    )
}

export default ManufacturerForm;
