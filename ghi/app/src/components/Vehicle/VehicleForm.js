import { useState, useEffect } from 'react';

function VehicleForm() {
    const [models, setModels] = useState([])

    const getData = async () => {
        const url = 'http://localhost:8100/api/models/';
        const res = await fetch(url);
        if (res.ok) {
            const data = await res.json();
            setModels(data.models)
        }
    }

    useEffect(() => {
        getData();
    }, []);

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {}
        data.color = color
        data.year = year
        data.vin = vin
        data.model_id = model

        const url = 'http://localhost:8100/api/automobiles/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const res = await fetch(url, fetchConfig);
        if (res.ok) {
            const newVehicle = await res.json();
            setHasSignedUp(true)
            setColor(' ')
            setYear(' ')
            setVin(' ')
            setModel(' ')
        } else {
            setHasFailed(true)
        }
    }

    const [color, setColor] = useState('')
    const handleColorChange = (e) => {
        const value = e.target.value
        setColor(value)
    }

    const [year, setYear] = useState('')
    const handleYearChange = (e) => {
        const value = e.target.value
        setYear(value)
    }

    const [vin, setVin] = useState('')
    const handleVinChange = (e) => {
        const value = e.target.value
        setVin(value)
    }

    const [model, setModel] = useState('')
    const handleModelChange = (e) => {
        const value = e.target.value
        setModel(value)
    }

    const [hasSignedUp, setHasSignedUp] = useState(false)
    const formClasses = (!hasSignedUp) ? '' : 'd-none';
    const messageClasses = (!hasSignedUp) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';

    const [hasFailed, setHasFailed] = useState(false)
    const formClass = (!hasFailed) ? '' : 'd-none';
    const messageClass=(!hasFailed) ? 'alert alert-warning d-none mb-0' : 'alert alert-danger mb-0';

    return (
        <div className='container shadow'>
            <div className='card shadow p-3'>
                <form className={formClasses - formClass} onSubmit={handleSubmit}>
                    <h1 className="text-center">New Vehicle</h1>
                    <div className='form-floating row'>
                        <input onChange={handleColorChange} className='form-control' required name='color'></input>
                        <label>Color</label>
                    </div>
                    <div className='form-floating row'>
                        <input onChange={handleYearChange} className='form-control' required name='year'></input>
                        <label>Year</label>
                    </div>
                    <div className='form-floating row'>
                        <input onChange={handleVinChange} className='form-control' required name='vin'></input>
                        <label>Vin</label>
                    </div>
                    <div className='form-floating row'>
                        <select onChange={handleModelChange} value={model} required name='model' className='form-select'>
                            <option value=''>Choose Model</option>
                            {models.map(model => {
                                return (
                                    <option key={model.id} value={model.id}>{model.name}</option>
                                )
                            })}
                        </select>
                    </div>
                    <div className="p-2 row">
                        <button className="btn btn-primary">Save</button>
                    </div>
                </form>
                <div className={messageClasses}  id="success-message">
                    <p className="text-center">New Vehicle Added!</p>
                </div>
                <div className={messageClass} id="warning-message">
                    <p className="text-center">Unable To Create Vehicle!!!</p>
                    <p className="text-center">Vehicles's VIN May Already Be In Use!!!</p>
                </div>
            </div>
        </div>
    )
}

export default VehicleForm;
