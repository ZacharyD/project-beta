import { useEffect, useState } from "react"
import { useParams } from 'react-router-dom'

function SalesMemberList() {
    const [employees, setEmployees] = useState([])
    const { id } = useParams()

    const getData = async ()=> {
        const response = await fetch('http://localhost:8090/api/employees/')
        if (response.ok) {
            const data = await response.json()
            setEmployees(data.sales_person)
        }
    }

    const handleDelete = async (id) => {
        const response = await fetch(`http://localhost:8090/api/employees/${id}`, {
            method: "DELETE",
        })

        if (response.ok) {
            setEmployees(employees.filter((employee) => employee.id !== id))
        }
    }

    useEffect(() => {
        getData()
    }, [])

    return (
        <>
        <div>
            <h1 className="text-center">Sales Team</h1>
            <table className = "table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Employee's Name</th>
                        <th>Employee's Phone Number</th>
                    </tr>
                </thead>
                <tbody>
                    {employees.map(employee => {
                        return (
                            <tr key={ employee.id }>
                                <td>{ employee.name }</td>
                                <td>{ employee.employee_id}</td>
                                <td>
                                    <button onClick={() => handleDelete(employee.id)}>Delete</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
        </>
    )
}

export default SalesMemberList;
