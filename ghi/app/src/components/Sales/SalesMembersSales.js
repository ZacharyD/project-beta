import React, {useEffect, useState} from 'react';

function SalesPersonRecordList() {
  const [filterValue, setFilterValue] = useState("");
  const [sales, setSales] = useState([]);

  const getData = async () => {
    const url = 'http://localhost:8090/api/sales/'
    const response = await fetch(url)
    if (response.ok) {
        const data = await response.json();
        setSales(data.sales_record);
    }
  }

  const handleChange = (event) => {
    setFilterValue(event.target.value);
  }

  const filteredEmployee = () => {
    return sales.filter((sale) =>
    sale.sales_person.name.toLowerCase().includes(filterValue)
    )
  }

  useEffect (() => {
    getData()
  }, [])

  return (
    <div>
      <h1 className="text-center">Sales Team Accomplishments</h1>
      <input onChange={handleChange} placeholder="Search Employee"/>
        <table className="table table-striped">
          <thead>
          <tr>
            <th>Employee Name</th>
            <th>Employee ID</th>
            <th>Customer's Name</th>
            <th>Automobile's VIN</th>
            <th>Sales Price</th>
          </tr>
          </thead>
          <tbody>
            {filteredEmployee().map((sale) => {
              return (
              <tr>
                <td>{sale.sales_person.name }</td>
                <td>{sale.sales_person.employee_id }</td>
                <td>{sale.customer.name}</td>
                <td>{sale.automobile.vin}</td>
                <td>${sale.price}</td>
              </tr>
              )
            })}
          </tbody>
        </table>
    </div>
  )
}

export default SalesPersonRecordList;
