import React, { useState, useEffect } from 'react'

function EmployeeForm () {
    const [formData, setFormData] = useState({
        name: '',
        employee_id: '',
    })

    const handleFormChange = e => {
        setFormData({
            ...formData,
            [e.target.name]:e.target.value
        })
    }

    const handleSubmit = async event => {
        event.preventDefault()
        const url = 'http://localhost:8090/api/employees/'
        const fetchConfig = {
            method:"post",
            body: JSON.stringify(formData),
            headers: {"Content-Type": "application/json"}
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            const newResponse = await response.json()

            setFormData({
                name:'',
                employee_id:''
            })
            setHasSignedUp(true)
        } else {
            setHasFailed(true)
        }
    }

    const [hasFailed, setHasFailed] = useState(false)
    const formClass = (!hasFailed) ? '' : 'd-none';
    const messageClass=(!hasFailed) ? 'alert alert-warning d-none mb-0' : 'alert alert-danger mb-0';

    const [hasSignedUp, setHasSignedUp] = useState(false)
    const formClasses = (!hasSignedUp) ? '' : 'd-none';
    const messageClasses = (!hasSignedUp) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1 className="text-center">Add a sales member</h1>
                    <form className={formClasses - formClass} onSubmit={handleSubmit} id="create-salesMember-form" >
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={ formData.name } placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={ formData.employee_id } placeholder="Employee ID" required type="text" name="employee_id" id="employee_id" className="form-control"/>
                        <label htmlFor="employee_ID">Employee ID</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                    <div className={messageClass} id="warning-message">
                    <p className="text-center">Sales member ID already in use! Please try another ID</p>
                    </div>
                    <div className={messageClasses} id="success-message">
                    <p className="text-center">Sales member has been created!</p>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default EmployeeForm;
