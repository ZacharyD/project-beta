import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';

function AppointmentList() {
    const [appointments, setAppointments] = useState([]);
    const { id } = useParams();

    useEffect(() => {
        async function fetchData() {
            const response = await fetch('http://localhost:8080/api/appt/');
            const data = await response.json();
            setAppointments(data.appts)
        }
        fetchData();
    }, []);

    const [vips, setVip] = useState([])
    const getVipStatus = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/')

        if (response.ok) {
            const JsonData = await response.json()
            setVip(JsonData.autos)
        }
    }

    useEffect(() => {
        getVipStatus()
    }, []);

    function handleCancel(appointment) {
        fetch(`http://localhost:8080/api/appt/${appointment.id}/`, {
            method: "DELETE",
        })
        .then(response => response.json())
        .then(data => {
            const updatedAppointments = appointments.filter(appt => appt.id !== appointment.id);
            setAppointments(updatedAppointments);
        })
        .catch(error => {
            console.log(error);
        });
    }

    function handleCompleted(appointment) {
        fetch(`http://localhost:8080/api/appt/${appointment.id}/`, {
            method: "PUT",
            body: JSON.stringify({status: true}),
            headers: {
                'Content-Type': 'application.json'
            }
        })
        .then(response => response.json())
        .then(data => {
            const updatedAppointments = appointments.map(appt => {
                if(appt.id === appointment.id) {
                    return {...appt, status: false};
                }
                return appt;
            });
            console.log(updatedAppointments)
            setAppointments(updatedAppointments);
        })
        .catch(error => {
            console.log(error)
        });
    }

    return (
        <>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Vin</th>
                        <th>Customer Name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>VIP Status</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => (
                        <tr key={appointment.id}>
                            <td>{appointment.vin}</td>
                            <td>{appointment.cust_name}</td>
                            <td>{appointment.date}</td>
                            <td>{appointment.time}</td>
                            <td>{appointment.technician.name}</td>
                            <td>{appointment.reason}</td>
                            <td>
                                {vips.find(auto =>
                                    auto.vin === appointment.vin) ? "Vip" : "Not Vip"}
                            </td>
                            <td>
                                <button className='btn btn-danger' onClick={() =>
                                handleCancel(appointment)}>Cancel</button>
                                <button className='btn btn-success' onClick={() =>
                                handleCompleted(appointment)}>Completed</button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </>
    )
}
export default AppointmentList;
