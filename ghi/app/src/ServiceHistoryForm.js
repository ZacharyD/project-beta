import React, { useState, useEffect } from 'react';

function ServiceHistoryForm() {
  const [appts, setAppts] = useState([]);
  const [filteredData, setFilteredData] = useState(' ');

    const fetchData = async () => {
        const response = await fetch('http://localhost:8080/api/appt/');

        if (response.ok) {
          const data = await response.json();
          setAppts(data.appts);
        }
    }

    const handleChange = (e) => {
      setFilteredData(e.target.value);
    }

    const filteredService = () => {
      return appts.filter((appt) =>
      appt.vin.toLowerCase().includes(filteredData))
    }

    useEffect (() => {
      fetchData()
    }, []);

  return (
    <div>
      <input
        type="text"
        onChange={handleChange}
        placeholder="Search by VIN"
      />
      <table className='table table-striped'>
        <thead>
          <tr>
            <th>VIN</th>
            <th>Customer Name</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
          </tr>
        </thead>
        <tbody>
          {filteredService().map(appt => (
            <tr key={appt.vin}>
              <td>{appt.vin}</td>
              <td>{appt.cust_name}</td>
              <td>{appt.date}</td>
              <td>{appt.time}</td>
              <td>{appt.technician.name}</td>
              <td>{appt.reason}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ServiceHistoryForm;
